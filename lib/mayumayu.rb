# coding: utf-8

require 'mayumayu/version'
require 'readline'
require 'date'

class Mayu
  def self.name
    'mayu'
  end

  def self.account
    'kae_kasui'
  end

  def self.birthday
    Date.new(1985, 7, 6)
  end

  def self.three_size
    # TODO: needs pull-request
    raise NotImplementedError
  end
end

class Date
  def self.birthday_of_mayu?
    today.month == Mayu.birthday.month &&
    today.day == Mayu.birthday.day
  end
end

class Greetings
  def self.show
    puts <<-EOD
　　　 o ◇ ◎｡o✨｡
　　 ｡✨ﾟ ∧ ∧  ☆｡◎
　　/o○(*ﾟーﾟ) ◇☆
　 / |￣￣∪∪ ￣￣|
　/ ﾟ|☆for mayu☆ |
▲　ﾟ◇o☆＿＿＿＿＿|
□▼――------☆ ∂ ✨ ◎


🎀  HAPPY BIRTHDAY !! 🎀

    🔥  🔥  🔥  🔥
    || || || ||
　　/⌒ ⌒ ⌒ ⌒ ⌒ヽ
　 (_ﾉ υ^υ^υ^ ﾋ)
　　|=========|
　 /⌒ ⌒ ⌒⌒⌒ ⌒ ⌒ヽ  
 （_ﾉ^ι^υ^υ^υ^ ﾋ)
　 |=o=o=o==o==|
　 |○ ○ ○ ○ ○ ○|
 ☆^~~~~~~~~~~~~~~^☆
￣￣￣￣￣￣￣￣￣￣
    EOD

    puts "\npress enter key!"
    Readline.readline
  end

  def self.reply
    system('open https://twitter.com/intent/tweet?text=@nicklegr%20')
  end
end
