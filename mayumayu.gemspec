# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mayumayu/version'

Gem::Specification.new do |gem|
  gem.name          = "mayumayu"
  gem.version       = Mayumayu::VERSION
  gem.authors       = ["Nickle"]
  gem.email         = ["nickle-github@nickle.ath.cx"]
  gem.description   = "happy birthday!"
  gem.summary       = "present for you"
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
